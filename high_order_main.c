#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <errno.h>
#include "linked_list.h"
#include "io_list.h"
#include "high_order_functions.h"
#include "int64t_rw.h"

const char *filename = "./listfile";


static void write_value_new_line(int64_t value) {
    printf("%" PRId64 " \n", value);
}


static void read_value_in_list(struct LinkedListNode **list) {
    int64_t value;
    value = read_int64t();
    list_add_front(list, value);

}

static int64_t inc(int64_t value) {

    return value + 1;
}


static int64_t dec(int64_t value) {

    return value - 1;
}

static int64_t sum(int64_t x, int64_t a) {

    return x + a;
}

static int64_t cube(int64_t value) {
    return value * value * value;
}

static int64_t square(int64_t value) {
    return value * value;
}

static int64_t max_element(int64_t value, int64_t a) {
    return value > a ? value : a;
}

static int64_t min_element(int64_t value, int64_t a) {
    return value < a ? value : a;
}

static int64_t power(int64_t value) {
    return value * 2;
}

static int64_t myabs(int64_t value) {
    return value >= 0 ? value : (value * -1);
}


int main() {
    /*
    struct LinkedListNode* tempNode;
    int64_t tempElement;
    */
    struct LinkedListNode **list;
    struct LinkedListNode *head;
    struct LinkedListNode *tempList;
    int64_t value;
    FILE *file;

    puts("введите числа: ");
    value = read_int64t();
    head = list_create(value);
    list = &head;

    read_list(list);


    list_add_after(&head, 123);

    /* Тест  foreach*/
    puts("foreach write space: ");
    foreach(head, write_int64t);
    puts("");

    /* Тест  foreach*/
    puts("foreach new line: ");
    foreach(head, write_value_new_line);
    puts("");




    /* Тест  map*/
    puts("map square: ");
    tempList = map(head, square);
    foreach(tempList, write_int64t);
    puts("");

    /* Тест  map*/
    puts("map cube: ");
    tempList = map(head, cube);
    foreach(tempList, write_int64t);
    puts("");

    /* Тест  map_mut*/
    puts("map_mut abs: ");
    head = map_mut(head, myabs);
    foreach(head, write_int64t);
    puts("");

    /* Тест  foldl sum*/
    puts("map_mut sum: ");
    value = 0;
    value = foldl(head, value, sum);
    write_int64t(value);
    puts("");

    /* Тест  foldl min_element*/
    puts("foldl min_element: ");
    value = head[0].value;
    value = foldl(head, value, min_element);
    write_int64t(value);
    puts("");

    /* Тест  foldl max_element*/
    puts("foldl max_element: ");
    value = 0;
    value = foldl(head, value, max_element);
    write_int64t(value);
    puts("");

    /* Тест  iterate inc value*/
    puts("iterate power value: ");
    value = 1;
    tempList = iterate(value, 5, power);
    foreach(tempList, write_int64t);
    puts("");


    puts("save load: ");

    file = open_file(filename, "w+");
    save(tempList, file);
    rewind(file);
    load(&tempList, file);
    close_file(file, filename);

    foreach(tempList, write_int64t);
    puts("");


    puts("serialize deserialize: ");
    file = open_file(filename, "w+");
    serialize(tempList, file);
    rewind(file);
    deserialize(&tempList, file);
    foreach(tempList, write_int64t);
    close_file(file, filename);
    puts("");


    puts("");
    list_free(head);
    list_free(tempList);
    return 0;
}