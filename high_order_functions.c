#include <errno.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdio.h>
#include "high_order_functions.h"
#include "linked_list.h"

void foreach(struct LinkedListNode const *const head, void (action)(int64_t)) {
    struct LinkedListNode *iter;
    for (iter = head; iter != NULL; iter = iter->next)
        action(iter->value);
}

struct LinkedListNode *map(struct LinkedListNode const *const head, int64_t (action)(int64_t)) {

    struct LinkedListNode *new_iter = NULL;
    struct LinkedListNode *start = NULL;

    for (struct LinkedListNode const *iter = head; iter != NULL; iter = iter->next) {
        new_iter = list_add_back(&new_iter, action(iter->value));
        if (start == NULL) start = new_iter;
    }

    return start;
}

struct LinkedListNode *map_mut(struct LinkedListNode *list, int64_t (action)(int64_t)) {

    for (struct LinkedListNode *iter = list; iter != NULL; iter = iter->next)
        iter->value = action(iter->value);

    return list;
}

int64_t foldl(const struct LinkedListNode *const head, int64_t acc, int64_t (action)(int64_t, int64_t)) {

    if (head == NULL) return NULL;

    for (struct LinkedListNode const *iter = head; iter != NULL; iter = iter->next)
        acc = action(iter->value, acc);

    return acc;
}

struct LinkedListNode *iterate(int64_t value, const size_t length, int64_t (action)(int64_t)) {
    size_t i;
    struct LinkedListNode *start, *iter;

    start = list_create(value);
    iter = start;

    for (i = 1; i < length; i++) {
        value = action(value);
        iter = list_add_back(&iter, value);
    }

    return start;
}

int64_t save(struct LinkedListNode const *const head, FILE *f) {
    struct LinkedListNode *iter;
    errno = 0;
    for (iter = (struct LinkedListNode *) head; iter != NULL; iter = iter->next) {
        fprintf(f, "%" PRId64 " ", iter->value);
        if (errno) return 0;
    }

    return 1;
}

int64_t load(struct LinkedListNode **list, FILE *f) {
    struct LinkedListNode *iter = NULL, *start = NULL;
    int64_t value;

    errno = 0;

    while (1) {
        fscanf(f, "%" SCNd64, &value);
        if (feof(f)) break;

        if (errno || ferror(f)) {

            return 0;
        }

        iter = list_add_after(&iter, value);

        if (start == NULL) start = iter;
    }

    *list = start;
    return 1;
}


int64_t serialize(struct LinkedListNode const *const list, FILE *f) {
    struct LinkedListNode *iter;

    errno = 0;

    if (errno) return 0;

    for (iter = (struct LinkedListNode *) list; iter != NULL; iter = iter->next) {
        fwrite(&iter->value, sizeof(int64_t), 1, f);
        if (errno || ferror(f)) return 0;

    }

    return 1;
}

int64_t deserialize(struct LinkedListNode **list, FILE *f) {
    struct LinkedListNode *iter = NULL, *start = NULL;
    int64_t value;
    errno = 0;

    while (1) {
        fread(&value, sizeof(int64_t), 1, f);
        if (feof(f)) break;

        if (errno || ferror(f)) return 0;

        iter = list_add_after(&iter, value);
        if (start == NULL) start = iter;
    }

    *list = start;
    return 1;
}



