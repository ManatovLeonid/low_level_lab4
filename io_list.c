
#include <stdio.h>
#include <inttypes.h>

#include <errno.h>
#include "linked_list.h"
#include "io_list.h"
void read_list(struct LinkedListNode** list)
{
	int64_t value;	
	while(scanf("%" SCNd64, &value) != EOF)
		list_add_front(list, value);
	
}

void write_list(struct LinkedListNode const * const head)
{
	struct LinkedListNode* iter;
		
	for (iter = (struct LinkedListNode*)head; iter != NULL; iter = iter->next)	
		printf("%" PRId64 " ", iter->value);		
	
	
	puts("");
}


FILE *open_file(const char *filename, const char *mode) {

    FILE *file;
    file = fopen(filename, mode);

    if (errno != 0) {
        printf("The file '%s'  was not opened\n", filename);
        return NULL;
    }

    return file;
}

void close_file(FILE *file,const char *filename) {

    fclose(file);
    if (errno != 0) printf("The file '%s'  was not closed\n", filename);

}