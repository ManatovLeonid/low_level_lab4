#ifndef _IO_LIST_H_
#define _IO_LIST_H_

#include "linked_list.h"

void read_list(struct LinkedListNode **list);


void write_list(struct LinkedListNode const *const head);

FILE *open_file(const char *filename, const char *mode);

void close_file(FILE *file, const char *filename);

#endif
