#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

int64_t read_int64t() {
    int64_t value;
    scanf("%" SCNd64, &value);
    return value;
}

void write_int64t(int64_t value) {
    printf("%" PRId64 " ", value);
}
