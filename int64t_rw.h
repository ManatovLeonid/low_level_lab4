
#ifndef LINKEDLIST_INT64T_RW_H
#define LINKEDLIST_INT64T_RW_H

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

int64_t read_int64t();
void write_int64t(int64_t value);

#endif //LINKEDLIST_INT64T_RW_H
