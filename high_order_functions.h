#ifndef _HIGH_ORDER_FUNCTIONS_H_
#define _HIGH_ORDER_FUNCTIONS_H_
#include <inttypes.h>
#include <stdio.h>
#include "linked_list.h"

void foreach( struct LinkedListNode const * const head, void (action)(int64_t));

struct LinkedListNode *map(struct LinkedListNode const * const head, int64_t (action)(int64_t));

struct LinkedListNode *map_mut(struct LinkedListNode *list, int64_t (action)(int64_t)) ;

int64_t foldl(const struct LinkedListNode *const head, int64_t acc, int64_t (action)(int64_t, int64_t));

struct LinkedListNode *iterate(int64_t value,const size_t length, int64_t (action)(int64_t));

int64_t save(struct LinkedListNode const * const head, FILE *f);
 
int64_t load(struct LinkedListNode **list,  FILE *f);
 

int64_t serialize(struct LinkedListNode const * const list, FILE *f);
 

int64_t deserialize(struct LinkedListNode **list, FILE *f);
 
#endif