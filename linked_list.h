#ifndef _LINKED_LIST_H_
#define _LINKED_LIST_H_

#include <stdlib.h>
#include <inttypes.h>

struct LinkedListNode {
    int64_t value;
    struct LinkedListNode *next;
};

struct LinkedListNode *list_create(const int64_t number);

struct LinkedListNode *list_add_front(struct LinkedListNode **list, const int64_t number);

struct LinkedListNode *list_add_back(struct LinkedListNode **list, const int64_t number);

struct LinkedListNode *list_add_after(struct LinkedListNode **list, const int64_t number);

int64_t list_get(const struct LinkedListNode *const head, const size_t id);

void list_free(struct LinkedListNode *head);

size_t list_length(const struct LinkedListNode *const head);

struct LinkedListNode *list_node_at(struct LinkedListNode *head, const size_t id);


int64_t list_sum(const struct LinkedListNode *const head);

#endif
