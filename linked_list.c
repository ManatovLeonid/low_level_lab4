
#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include "linked_list.h"

struct LinkedListNode *list_create(const int64_t number) {
    struct LinkedListNode *node = malloc(sizeof(struct LinkedListNode));
    node->value = number;
    node->next = NULL;


    return node;

}

struct LinkedListNode *list_add_front(struct LinkedListNode **list, const int64_t number) {
    struct LinkedListNode *node = list_create(number);
    node->next = *list;
    *list = node;
    return node;

}

struct LinkedListNode *list_add_after(struct LinkedListNode **list, const int64_t number) {
    struct LinkedListNode *node = list_create(number);
    if (*list == NULL) {
        *list = node;
        return node;
    } else {
        node->next = (*list)->next;
        (*list)->next = node;
    }

    return node;

}


struct LinkedListNode *list_add_back(struct LinkedListNode **list, const int64_t number) {
    struct LinkedListNode *node = list_create(number);

    if (*list == NULL) {
        *list = node;
    } else {
        struct LinkedListNode *iter = *list;

        for (iter = *list; iter->next != NULL; iter = iter->next);
        iter->next = node;
    }

    return node;


}


struct LinkedListNode *list_node_at(struct LinkedListNode *head, const size_t id) {

    size_t i;
    struct LinkedListNode *iter = head;
    if (iter == NULL) return NULL;


    for (i = 0; i < id; i++) {
        if (iter->next == NULL) return NULL;
        iter = iter->next;
    }

    return iter;

}


int64_t list_get(const struct LinkedListNode *const head, const size_t id) {
    struct LinkedListNode *iter = list_node_at((struct LinkedListNode *) head, id);
    if (iter == NULL) return 0;
    return iter->value;

}

size_t list_length(const struct LinkedListNode *const head) {

    struct LinkedListNode *iter;
    size_t length = 0;
    for (iter = (struct LinkedListNode *) head; iter != NULL; iter = iter->next)
        length++;
    return length;

}

int64_t list_sum(const struct LinkedListNode *const head) {
    struct LinkedListNode *iter;
    int64_t sum = 0;
    for (iter = (struct LinkedListNode *) head; iter != NULL; iter = iter->next)
        sum += (int64_t) iter->value;
    return sum;

}


void list_free(struct LinkedListNode *head) {
    struct LinkedListNode *iter = (struct LinkedListNode *) head;

    while (iter != NULL) {
        struct LinkedListNode *prev = iter;
        iter = iter->next;
        free(prev);
    }
}
