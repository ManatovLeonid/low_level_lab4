#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include "io_list.h"
#include "linked_list.h"

int main() 
{
	struct LinkedListNode* tempNode;
	int64_t tempElement;
	
	struct LinkedListNode** list;
	struct LinkedListNode* head;
	
	int64_t value;	
	scanf("%" SCNd64, &value);
	head= list_create(value);
	list=&head;
	
	
	/* Тест  list_add_front*/
	read_list(list);
	
	/* Тест  list_add_back*/
	puts("list_add_back: ");
	scanf("%" SCNd64" \n", &value);
	list_add_back(list,value);
		
	/* Тест  list_node_at*/
	puts("введите id: ");
	scanf("%" SCNd64, &value);
	tempNode=list_node_at(head,value);
	puts("list_node_at id: ");
	printf("%" PRId64" \n", tempNode->value);
	
	/* Тест  list_get*/
	tempElement=list_get(head,value);
	puts("list_get at id: ");
	printf("%" PRId64"\n", tempElement);
		
	/* Тест  list_length*/
	puts("list_length: ");
	tempElement=list_length(head);
	printf("%" PRId64" \n", tempElement);
	
	write_list(head);
	
	
	/* Тест  list_sum*/
	puts("list_sum: ");
	tempElement=list_sum(head);
	printf("%" PRId64" \n", tempElement);
	
	
	list_free(head);
    return 0;
}
